const { book } = require('./models')       

book.create({                  
    isbn: '979-780-056-8',
    judul: 'Cinta Brontosaurus',
    sinopsis:'Sebuah cerita-cerita pendek',
    penulis: 'Raditya Dika',
    genre: 'Komedi'
})
.then(book => { 
    console.log(book)  
})