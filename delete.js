const { book } = require('./models')

book.destroy({
    where: { id: 1 }
})
    .then(() => console.log("Buku dengan id 1 sudah dihapus"))